package com.ef.log.repository;

import com.ef.log.helper.LogsHelper;
import com.ef.log.repository.database.DatabaseConnection;

import java.sql.*;
import java.util.ArrayList;

/**
 * @author : arafat
 * @version : 1.0.0-snapshot
 * @description ...
 * @project : Parser
 * @since : 7/19/18
 */
public class AccessLogRepository {
    LogsHelper logsHelper = new LogsHelper();
    Connection connection = null;
    public AccessLogRepository(DatabaseConnection databaseConnection) {
        this.connection = databaseConnection.getConnection();
    }

    public boolean storedAccessLogs(ArrayList<Log> accessLogs) {
        boolean isStoredSuccess = true;
        try {
            if (connection != null && !connection.isClosed()) {
                PreparedStatement preparedStatement = null;
                for (Log log: accessLogs
                        ) {
                    /**
                     * Stored General Info regarding IP blocked in BlockedIP table.
                     */
                    preparedStatement = connection.prepareStatement("INSERT INTO BlockedIP (ip, totalRequest, messages) VALUES (?,?,?)");
                    if(preparedStatement != null){
                        preparedStatement.setString(1, log.getIP().getHostAddress());
                        preparedStatement.setInt(2, log.getTotalRequest());
                        preparedStatement.setString(3, log.getMessage());
                        preparedStatement.executeUpdate();
                        preparedStatement.close();

                        /**
                         * Stored Access Logs in AccessLogs table.
                         */
                        preparedStatement = connection.prepareStatement("INSERT INTO AccessLogs (ip, startDate, endDate, request, status, userAgent) VALUES (?,?,?,?,?,?)");
                        if(preparedStatement != null){
                            preparedStatement.setString(1, log.getIP().getHostAddress());
                            preparedStatement.setTimestamp(2, new Timestamp(log.getStartDate().getTime()));
                            preparedStatement.setTimestamp(3, new Timestamp(log.getEndDate().getTime()));
                            preparedStatement.setString(4, log.getRequest());
                            preparedStatement.setInt(5, log.getStatus());
                            preparedStatement.setString(6, log.getUserAgent());
                            preparedStatement.executeUpdate();
                            preparedStatement.close();
                        }
                    }
                }
            }else {
                isStoredSuccess = false;
            }
        } catch (SQLException e) {
            isStoredSuccess = false;
            System.out.println("Database Query Execution Error\n------------------------------\n"+e.getMessage()+"\n------------------------------");
            e.printStackTrace();
        }
        return isStoredSuccess;
    }
}
