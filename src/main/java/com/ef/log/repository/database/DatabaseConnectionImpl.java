package com.ef.log.repository.database;

import com.ef.log.helper.LogsHelper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * @author : arafat
 * @version : 1.0.0-snapshot
 * @description ...
 * @project : Parser
 * @since : 7/23/18
 */
public class DatabaseConnectionImpl implements DatabaseConnection {
    LogsHelper logsHelper = new LogsHelper();
    Connection connection = null;
    boolean isDatabaseInfoAddedManually = false;
    boolean isDatabaseExist = true;
    String databaseURL = "jdbc:mysql://localhost:3306/test";
    String userName = "root";
    String password = "root";

    @Override
    public void initializeConnection() {
        try {
            if(!isDatabaseInfoAddedManually){
                setDatabaseInfo();
            }
            if(isDatabaseExist){
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection(
                        databaseURL, userName, password);
                if (connection != null && !connection.isClosed()) {
                    System.out.println("Database Connection Established!");
                } else {
                    System.out.println("Database Connection Failed!");
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean closeConnection() throws SQLException {
        if(connection != null){
            connection.close();
            if(connection.isClosed()){
                System.out.println("Database Connection Closed!");
                return true;
            }
        }
        return false;
    }

    private void setDatabaseInfo() {
        String choice;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Is MySQL running in your system? Enter Y/N for Yes/No! MySQL must exist with test database.");
        choice = logsHelper.removeExtraSpaces(scanner.nextLine());
        if (choice.equalsIgnoreCase("Y")) {
            System.out.println("Do you want enter database info here? Enter Y/N for Yes/No!\nIf you enter N then this system will use default info{URL:" + databaseURL + ", username:" + userName + ", password:" + password+"}.");
            choice = logsHelper.removeExtraSpaces(scanner.nextLine());
            if (choice.equalsIgnoreCase("Y")) {
                System.out.println("### Enter Database URL ###");
                databaseURL = logsHelper.removeExtraSpaces(scanner.nextLine());
                System.out.println("### Enter Username ###");
                userName = logsHelper.removeExtraSpaces(scanner.nextLine());
                System.out.println("### Enter Password ###");
                password = logsHelper.removeExtraSpaces(scanner.nextLine());
                isDatabaseInfoAddedManually = true;
            }
        }else {
            isDatabaseExist = false;
            isDatabaseInfoAddedManually = false;
        }
        scanner.close();
    }

    @Override
    public Connection getConnection(){
        if(connection == null){
            initializeConnection();
        }
        return connection;
    }
}
