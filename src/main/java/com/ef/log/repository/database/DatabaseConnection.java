package com.ef.log.repository.database;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author : arafat
 * @version : 1.0.0-snapshot
 * @description ...
 * @project : Parser
 * @since : 7/22/18
 */
public interface DatabaseConnection {
    void initializeConnection();
    Connection getConnection();
    boolean closeConnection() throws SQLException;

}
