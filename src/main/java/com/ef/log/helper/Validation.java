package com.ef.log.helper;

import com.ef.log.reader.Duration;
import com.ef.log.reader.InputData;

import java.io.File;

/**
 * @author : arafat
 * @version : 1.0.0-snapshot
 * @description ...
 * @project : Parser
 * @since : 7/18/18
 */
public class Validation {
    public boolean checkArgumentsValidity(InputData inputData){
        System.out.println("Checking Arguments Validity ...");
        if(isFileExist(inputData)){
            if(isValidDuration(inputData.getDuration())){
                return true;
            }
        }
        return false;
    }
    private boolean isFileExist(InputData inputData){
        File logFile = new File(inputData.getLogFilePath());
        if(logFile.exists() && !logFile.isDirectory()){
            inputData.setLogFile(logFile);
            return true;
        }
        System.out.println("File Not Found! Either wrong file location or file already removed/moved!!!");
        return false;
    }
    private boolean isValidDuration(String duration){
        if(duration.equals(Duration.hourly.toString()) || duration.equals(Duration.daily.toString())){
            return true;
        }
        System.out.println("Duration is not valid!");
        return false;
    }
}
