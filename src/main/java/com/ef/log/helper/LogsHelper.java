package com.ef.log.helper;

import com.ef.log.reader.InputData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author : arafat
 * @version : 1.0.0-snapshot
 * @description ...
 * @project : Parser
 * @since : 7/18/18
 */
public class LogsHelper {
    public InputData argumentsToInputData(String[] commandArgs){
        InputData inputData = new InputData();
        for (int i=0; i<commandArgs.length; i++){
            String[] argumentPartition = commandArgs[i].split("=");
            String argumentKey = argumentPartition[0];
            if(argumentKey.startsWith("--accesslog")){
                inputData.setLogFilePath(argumentPartition[1]);
            }else if(argumentKey.startsWith("--startDate")){
                try {
                    inputData.setStartDate(new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss").parse(argumentPartition[1]));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }else if(argumentKey.startsWith("--duration")){
                inputData.setDuration(argumentPartition[1]);
            }else if(argumentKey.startsWith("--threshold")){
                inputData.setThreshold(Integer.parseInt(argumentPartition[1]));
            }
        }
        return inputData;
    }
    public boolean validateArguments(InputData inputData){
        return new Validation().checkArgumentsValidity(inputData);
    }
    public Date findEndDate(Date startDate, int hours){
        int seconds = (hours * 60 * 60) - 1;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.add(Calendar.SECOND, seconds);
        return calendar.getTime();
    }
    public boolean isValidDuration(Date startDate, Date endDate, Date logDate){
         if (logDate.after(startDate) && logDate.before(endDate)){
             return true;
         }
         return false;
    }
    public String removeExtraSpaces(String text){
        return text.replaceAll("\\s+$", "");
    }
}
