package com.ef.log.reader;

/**
 * Created by arafat on 7/18/18.
 */
public enum Duration {
    hourly, daily;
}
