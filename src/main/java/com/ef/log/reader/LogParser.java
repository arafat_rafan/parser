package com.ef.log.reader;

import com.ef.log.repository.Log;

import java.io.FileNotFoundException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * @author : arafat
 * @version : 1.0.0-snapshot
 * @description ...
 * @project : Parser
 * @since : 7/18/18
 */
public interface LogParser {
    ArrayList<Log> findIP(InputData inputData) throws FileNotFoundException, ParseException, UnknownHostException;
}
