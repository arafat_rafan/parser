package com.ef.log.reader;

import com.ef.log.helper.LogsHelper;
import com.ef.log.repository.Log;

import java.io.FileNotFoundException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author : arafat
 * @version : 1.0.0-snapshot
 * @description ...
 * @project : Parser
 * @since : 7/18/18
 */
public class LogParserImpl implements LogParser {
    private HashMap<String, ArrayList<Log>> logs;
    InputData inputData = null;
    LogsHelper logsHelper = new LogsHelper();
    
    @Override
    public ArrayList<Log> findIP(InputData inputData) throws FileNotFoundException, ParseException, UnknownHostException {
        if (inputData != null){
            this.inputData = inputData;
            if(parseFile()){
                return detectIPs();
            }
        }
        return null;
    }
    private boolean parseFile() throws FileNotFoundException, ParseException, UnknownHostException {
        Scanner scanner = new Scanner(inputData.getLogFile());
        scanner = scanner.useDelimiter("|");
        logs = new HashMap<>();
        System.out.println("File is reading ....");
        while (scanner.hasNext()){
            StringTokenizer stringTokenizer = new StringTokenizer(scanner.nextLine(), "|");
            int totalTokens = stringTokenizer.countTokens();
            Log log = new Log();
            for (int i = 0; i < totalTokens; i++){
                switch (i){
                    case 0:
                        log.setStartDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(stringTokenizer.nextToken()));
                        break;
                    case 1:
                        String token = stringTokenizer.nextToken();
                        log.setIP(InetAddress.getByName(token));
                        break;
                    case 2:
                        log.setRequest(stringTokenizer.nextToken());
                        break;
                    case 3:
                        log.setStatus(Integer.parseInt(stringTokenizer.nextToken()));
                        break;
                    case 4:
                        log.setUserAgent(stringTokenizer.nextToken());
                        break;
                    default:
                        log.setMessage("Wrong Format Access Log");
                }
            }
            storeLogs(log);
        }
        System.out.println("File reading completed!");
        if(logs.size() > 0){
            return true;
        }
        return false;
    }

    private void storeLogs(Log log){
        if(logs.get(log.getIP().getHostAddress()) != null){
            ArrayList<Log> storedLogs = logs.get(log.getIP().getHostAddress());
            storedLogs.add(log);
            logs.put(log.getIP().getHostAddress(), storedLogs);
        }else{
            ArrayList<Log> newLogs = new ArrayList<>();
            newLogs.add(log);
            logs.put(log.getIP().getHostAddress(), newLogs);
        }
    }
    
    private ArrayList<Log> detectIPs(){
        ArrayList<Log> blockedIPsLog = null;
        System.out.println("Detecting blocked ip ...");
        for(Map.Entry<String, ArrayList<Log>> entry : logs.entrySet()) {
            String IP = entry.getKey();
            ArrayList<Log> storedLogs = entry.getValue();
            int totalRequest = 0;
            for (Log log: storedLogs
                 ) {
                int duration = inputData.getDuration().equals(Duration.hourly.toString())? 1: 24;
                if (logsHelper.isValidDuration(inputData.getStartDate(), logsHelper.findEndDate(inputData.getStartDate(), duration), log.getStartDate())){
                    totalRequest++;
                }
            }
            if(totalRequest > inputData.getThreshold()){
                if (blockedIPsLog == null){
                    blockedIPsLog = new ArrayList<>();
                }
                Date startDate = logs.get(IP).get(0).getStartDate();
                Log latestBlockedIPsLog = logs.get(IP).get(logs.get(IP).size() - 1);
                latestBlockedIPsLog.setEndDate(latestBlockedIPsLog.getStartDate());
                latestBlockedIPsLog.setStartDate(startDate);
                latestBlockedIPsLog.setTotalRequest(totalRequest);
                latestBlockedIPsLog.setMessage("Oops! This "+latestBlockedIPsLog.getIP().getHostAddress()+" address has been blocked due to too many requests!");
                blockedIPsLog.add(latestBlockedIPsLog);
            }
        }
        return blockedIPsLog;
    }
    
}