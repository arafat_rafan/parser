package com.ef.log.reader;

import java.io.File;
import java.util.Date;

/**
 * @author : arafat
 * @version : 1.0.0-snapshot
 * @description ...
 * @project : Parser
 * @since : 7/18/18
 */
public class InputData {

    private String logFilePath;
    private File logFile;
    private Date startDate;
    private String duration;
    private Integer threshold;

    public String getLogFilePath() {
        return logFilePath;
    }

    public void setLogFilePath(String logFilePath) {
        this.logFilePath = logFilePath;
    }

    public File getLogFile() {
        return logFile;
    }

    public void setLogFile(File logFile) {
        this.logFile = logFile;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Integer getThreshold() {
        return threshold;
    }

    public void setThreshold(Integer threshold) {
        this.threshold = threshold;
    }

    @Override
    public String toString() {
        return "InputData{" +
                "logFilePath='" + logFilePath + '\'' +
                ", logFile=" + logFile +
                ", startDate=" + startDate +
                ", duration='" + duration + '\'' +
                ", threshold=" + threshold +
                '}';
    }
}
