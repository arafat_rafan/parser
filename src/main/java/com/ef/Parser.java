package com.ef;

import com.ef.log.helper.LogsHelper;
import com.ef.log.reader.InputData;
import com.ef.log.reader.LogParser;
import com.ef.log.reader.LogParserImpl;
import com.ef.log.repository.AccessLogRepository;
import com.ef.log.repository.database.DatabaseConnection;
import com.ef.log.repository.database.DatabaseConnectionImpl;
import com.ef.log.repository.Log;

import java.io.FileNotFoundException;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

public class Parser {

    public static void main(String[] args) {
        InputData inputData = null;
        if (args.length > 0){
            LogsHelper logsHelper = new LogsHelper();
            inputData = logsHelper.argumentsToInputData(args);
            if(logsHelper.validateArguments(inputData)){
                findBlockedIPs(inputData);
            }
        }
    }

    private static void findBlockedIPs(InputData inputData){
        try {
            LogParser logParser = new LogParserImpl();
            ArrayList<Log> blockedIPs = null;
            try {
                blockedIPs = logParser.findIP(inputData);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            System.out.println("Detecting blocked ip Completed!");
            System.out.println("Total blocked IPs: "+blockedIPs.size());
            for (Log log: blockedIPs
                    ) {
                System.out.println(log.getIP().getHostAddress());
            }
            if(blockedIPs.size() > 0){
                loadLogsIntoDatabase(blockedIPs);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
    private static void loadLogsIntoDatabase(ArrayList<Log> blockedIPs){
        DatabaseConnection databaseConnection = new DatabaseConnectionImpl();
        AccessLogRepository accessLogRepository = new AccessLogRepository(databaseConnection);
        if(accessLogRepository.storedAccessLogs(blockedIPs)){
            System.out.println("Blocked IPs stored in Database!");
        }else{
            System.out.println("Blocked IPs stored in Database Failed!");
        }
        try {
            databaseConnection.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
